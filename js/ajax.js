(function ($, window, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.optastTest = {
    attach: function (context, settings) {
      // Extract the view DOM ID from the view classes.
      var classList = $('.view-optasy-test.view-id-optasy_test', context).attr('class').split(', ');
      var matches = /(js-view-dom-id-\w+)/.exec(classList);
      var currentViewId = matches[1].replace('js-view-dom-id-', 'views_dom_id:');
      var view = Drupal.views.instances[currentViewId];

      // Attaching view refresh on submit.
      // var nodeActionsForm = [];
      // $('input[type=submit]', '#node-form-actions').each(function (index) {
      //   var self_settings = $.extend({}, view.element_settings, {
      //     base: $(this).attr('id'),
      //     element: this
      //   });
      //
      //   nodeActionsForm[index] = Drupal.ajax(self_settings);
      // });
    }
  };

})(jQuery, this, Drupal, drupalSettings);
