<?php

namespace Drupal\optast_test\Plugin\views\area;

use Drupal\Core\Form\FormState;
use Drupal\optast_test\Form\NodeFormActions;
use Drupal\views\Annotation\ViewsArea;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Defines an area plugin to display a node form actions container.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("node_form_actions_container")
 */
class NodeFormActionsContainer extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $form_state = new FormState();
    $form = \Drupal::formBuilder()->buildForm(NodeFormActions::class, $form_state);

    $build = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'node-form-actions-container',
      ],
      'form' => $form,
    ];

    // Add custom library.
    $build['#attached']['library'][] = 'optast_test/ajax-node-actions-form';

    return $build;
  }

}
