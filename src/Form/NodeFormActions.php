<?php

namespace Drupal\optast_test\Form;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class NodeFormActions.
 */
class NodeFormActions extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_form_actions';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    if (!$node && ($node = $form_state->get('node'))) {
      $operation = 'update';
    }
    else {
      $operation = !$node ? 'create' : 'update';
    }

    $form['form_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $node ? $this->t('Edit @label', ['@label' => $node->label()]) : $this->t('Create new node'),
      '#weight' => -99,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Node title'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $node ? $node->label() : '',
      '#required' => TRUE,
    ];

    $has_body = $operation == 'update' ? $node->hasField('body') : TRUE;
    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Node Body'),
      '#default_value' => $node ? $node->body->value : '',
      '#rows' => 6,
      '#format' => $node ? $node->body->format : filter_default_format(),
      '#access' => $has_body,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Node type.'),
      '#options' => ['' => $this->t('- select -')] + node_type_get_names(),
      '#default_value' => $node ? $node->bundle() : '',
      '#required' => TRUE,
      // Restricting type field access since not a good idea to change node type
      // (bundle) since some fields might not exist on the changed bundle.
      // e.g. body field
      '#access' => !$node,
    ];

    if ($operation == 'update') {
      $status = $node->isPublished() ? 1 : 0;
    }
    else {
      $status = NULL;
    }
    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#description' => $this->t('Node status'),
      '#options' => [1 => $this->t('Published'), 0 => $this->t('Unpublished')],
      '#required' => TRUE,
      '#default_value' => $status,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $node ? $this->t('Save') : $this->t('Create'),
    ];

    // Adding cacheability metadata.
    $form_cacheability = new CacheableMetadata();
    if ($node) {
      $form_cacheability->addCacheableDependency($node);
    }
    $form_cacheability->applyTo($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node = $form_state->get('node');
    $values = $form_state->getValues();
    if (!$node) {
      $node = Node::create([
        'title' => $values['title'],
        'type' => $values['type'],
        'status' => $values['status'],
      ]);
      $message = $this->t('@title node successfully created', [
        '@title' => $values['title'],
      ]);
    }
    else {
      $node->set('title', $values['title']);
      $node->setPublished((bool) $values['status']);
      $message = $this->t('@title node successfully updated', [
        '@title' => $values['title'],
      ]);
    }

    // Setting body field in case node has it.
    if ($node->hasField('body')) {
      $node->set('body', $values['body']);
    }
    $node->save();
    drupal_set_message($message);
  }

}
