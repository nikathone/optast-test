<?php

namespace Drupal\optast_test\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class ScrollTopCommand implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * If the command is a response to a request from an #ajax form element then
   * this value can be NULL.
   *
   * @var string
   */
  protected $selector;

  /**
   * The duration of the animation.
   *
   * A string or number determining how long the animation will run.
   *
   * @var string|integer
   */
  protected $duration;

  /**
   * Constructs an SlideDownCommand object.
   *
   * @param string $selector
   *   A CSS selector.
   * @param string|integer $duration
   *   A string or number determining how long the animation will run.
   */
  public function __construct($selector, $duration = NULL) {
    $this->selector = $selector;
    $this->duration = $duration;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'scrollTop',
      'method' => NULL,
      'selector' => $this->selector,
      'duration' => $this->duration,
    ];
  }

}
