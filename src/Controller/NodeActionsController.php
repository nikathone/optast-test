<?php

namespace Drupal\optast_test\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormState;
use Drupal\node\NodeInterface;
use Drupal\optast_test\Form\NodeFormActions;
use Drupal\views\Ajax\ScrollTopCommand;

/**
 * Custom node edit/update action controller.
 */
class NodeActionsController extends ControllerBase {

  /**
   * Ajax node update.
   *
   * @param $method
   *   The method used in the request of this page.
   * @param \Drupal\node\NodeInterface $node
   *   The node to update.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|null|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Ajax response for the node actions form or a redirect to the basic node action form.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function ajaxNodeUpdate($method, NodeInterface $node) {
    $response = NULL;
    $redirect = TRUE;
    $parameters = ['node' => $node->id()];

    if ($method == 'ajax') {
      $redirect = FALSE;
      $form_state = new FormState();
      $form_state->set('node', $node);
      $node_edit_form = \Drupal::formBuilder()
        ->buildForm(NodeFormActions::class, $form_state, $node);

      // Creating an AjaxResponse.
      $response = new AjaxResponse();
      // Removing current form.
      $response->addCommand(new RemoveCommand('#node-form-actions'));
      // Making sure that we remove the previous appended update form.
      $response->addCommand(new RemoveCommand('#node-form-actions-container > div'));
      // Inserting node edit form.
      $response->addCommand(new AppendCommand('#node-form-actions-container', $node_edit_form));
      // Scrolling to form container.
      $response->addCommand(new ScrollTopCommand('#node-form-actions-container'));
    }

    if ($redirect) {
      // Redirecting to the basic form route.
      // @todo should add destination query parameter.
      $response = $this->redirect('optast_test.node_actions_form', $parameters);
    }

    return $response;
  }

}
