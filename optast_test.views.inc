<?php

/**
 * Implements hook_views_data_alter().
 */
function optast_test_views_data_alter(array &$data) {
  $data['node']['node_form_actions_container'] = [
    'title' => t('Node Form Actions Container'),
    'help' => t('Provides a form actions to the optast test page.'),
    'area' => [
      'id' => 'node_form_actions_container',
    ],
  ];
}
